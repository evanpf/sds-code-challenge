package com.Frabell.SDS.Rest;

import com.Frabell.SDS.Model.Inventory;
import com.Frabell.SDS.Model.InventoryDAO;
import com.Frabell.SDS.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("item")
public class Controller {

    @Autowired
    private InventoryDAO inventoryDAO;

    @Autowired
    private ItemService itemService;
    //Endpoint Mapping
    @GetMapping("/{itemID}")
    public Inventory getItem(@PathVariable long itemID, @RequestParam("daysFromNow") int daysFromNow) {
       // ResponseEntity<Inventory> res = ResponseEntity.ok(itemService.getItemValue(itemID, daysFromNow));
       // return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
         return itemService.getItemValue(itemID, daysFromNow);
    }

    @GetMapping("/workout")
    public String getDailyWorkout() {
        return "Run a hard 5k";
    }

}
