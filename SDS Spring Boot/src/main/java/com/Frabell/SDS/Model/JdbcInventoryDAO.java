package com.Frabell.SDS.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class JdbcInventoryDAO implements InventoryDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcInventoryDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}



    @Override
    public Inventory getItem(Long itemID) {
        String sqlSelectItem = "SELECT item, sell_date, quality FROM inventoryitems WHERE id = ?";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectItem, itemID);
        if(results.next()) {
            Inventory inventory = new Inventory();
            inventory.setItem(results.getString("item"));
            inventory.setSellTime(results.getInt("sell_date"));
            inventory.setQuality(results.getInt("quality"));
            return inventory;
        }
        return null;
    }


//    public Film getFilm(Long filmId) {
//		String sqlSelectAllFilms = "SELECT * FROM film WHERE film_id = ?";
//		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllFilms, filmId);
//		if(results.next()) {
//			Film film = new Film(results.getLong("film_id"));
//			film.setTitle(results.getString("title"));
//			film.setDescription(results.getString("description"));
//			film.setReleaseYear(results.getInt("release_year"));
//			film.setLength(results.getInt("length"));
//			film.setRating(results.getString("rating"));
//			return film;
//		}
//		return null;
//	}
}




