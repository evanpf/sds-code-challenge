package com.Frabell.SDS.Model;

public class Inventory {

    private Long id;
    private String item;
    private int sellTime;
    private int quality;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getSellTime() {
        return sellTime;
    }

    public void setSellTime(int sellTime) {
        this.sellTime = sellTime;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

}