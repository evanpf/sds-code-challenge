package com.Frabell.SDS.Service;


import com.Frabell.SDS.Model.Inventory;
import com.Frabell.SDS.Model.JdbcInventoryDAO;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

    private JdbcInventoryDAO jdbcInventoryDAO;

    ItemService(JdbcInventoryDAO jdbcInventoryDAO){
        this.jdbcInventoryDAO = jdbcInventoryDAO;
    }


    public Inventory getItemValue(Long id, int daysFromNow){
        Inventory inventory = this.jdbcInventoryDAO.getItem(id);

        inventory.setQuality(getQuality(inventory, daysFromNow));
        return inventory;
    }

    private int getQuality(Inventory inventory, int daysFromNow){
        int quality = 0;
        System.out.println("Inventory before mod " + inventory.getQuality());
        System.out.println("DAYS from now " + daysFromNow);
        switch(inventory.getItem()){
            case "Aged Brie":
                quality =  qualityIncrasesByOnePerDay(inventory.getQuality(), daysFromNow);
                break;
            case "Elixir of the Mongoose":
                quality = qualityDecrasesByOnePerDay(inventory.getQuality(), daysFromNow);
                break;
            default:
                quality = inventory.getQuality();
        }
        System.out.println("Quality after mod " + quality);
        return quality;
    }

    private int qualityDecrasesByOnePerDay(int quality, int daysFromNow){
        return quality - daysFromNow;
    }

    private int qualityIncrasesByOnePerDay(int quality, int daysFromNow){
        return quality + daysFromNow;
    }
}
