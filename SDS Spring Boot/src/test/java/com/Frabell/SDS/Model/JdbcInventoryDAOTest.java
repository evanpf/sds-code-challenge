package com.Frabell.SDS.Model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class JdbcInventoryDAOTest {

    @Autowired
    private JdbcInventoryDAO jdbc;

    @Test
    public void getItem() {
        Inventory result = jdbc.getItem((long) 2);
        assertEquals("Input: Aged Brie","Aged Brie", result.getItem());
        assertEquals("Input: Aged Brie",2, result.getSellTime());
        assertEquals("Input: Aged Brie",0, result.getQuality());
    }
}