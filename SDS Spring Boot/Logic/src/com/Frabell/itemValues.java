package com.Frabell;

public class itemValues {
    private int key;
    private String item;
    private int sellIn;
    private int quality;

    public itemValues(int key, String item, int sellIn, int quality){
        this.key=key;
        this.item = item;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void setSellIn(int sellIn) {
        this.sellIn = sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }
}
