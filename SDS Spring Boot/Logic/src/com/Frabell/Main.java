package com.Frabell;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        String userEntry;
        String userInput;
        int item;
        int daysFromNow;

        Scanner inputReader = new Scanner(System.in);

        String leftAlignFormat = "%-20s %-10s %-26s %-60s";
        System.out.format(leftAlignFormat, "#", "Item", "Days Until Expiration", "Quality Points");
        System.out.println();
        System.out.println("----------------------------------------------------------------------------");

        HashMap <Integer, List<Object>> values = inventoryStack();

        for (int i = 0; i <= values.size(); i++) {
            String brace = ")";
            if (values.containsKey(i)) {
                String itemName = values.get(i).subList(0, 1).toString().replace("[", "").replace("]", "");
                String days = values.get(i).subList(1, 2).toString().replace("[", "").replace("]", "");
                String quality = values.get(i).subList(2, 3).toString().replace("[", "").replace("]", "");
                System.out.format("%1d%1s%26s%15s%24s", i, brace, itemName, days, quality);
                System.out.println();
            }
        }

        System.out.println();
        System.out.print("Please enter an item number (1-5): ");

        userEntry = inputReader.nextLine();
        item = Integer.parseInt(userEntry);

        System.out.println();
        System.out.print("How many days in the future would you like to forecast the quality of selected item? (1-100):   ");

        userInput = inputReader.nextLine();
        daysFromNow = Integer.parseInt(userInput);

//        String expireParse = values.get(item).subList(1, 2).toString().replace("[", "").replace("]", "");
//        int daysLeft = Integer.parseInt(expireParse);
//
//        String qualityParse = values.get(item).subList(2, 3).toString().replace("[", "").replace("]", "");
//        int initialQuality = Integer.parseInt(qualityParse);

//        getQuality(item, daysLeft, initialQuality, daysFromNow);

    }

    public static HashMap inventoryStack() {
        LinkedHashMap<Integer, List<Object>> values = new LinkedHashMap<>();

        List<Object> vest = new ArrayList<>();
        vest.add("+5 Dexterity Vest");
        vest.add(10);
        vest.add(20);

        List<Object> brie = new ArrayList<>();
        brie.add("Aged Brie");
        brie.add(2);
        brie.add(0);

        List<Object> elixir = new ArrayList<>();
        elixir.add("Elixir of the Mongoose");
        elixir.add(5);
        elixir.add(7);

        List<Object> sulfuras = new ArrayList<>();
        sulfuras.add("Sulfuras");
        sulfuras.add(0);
        sulfuras.add(80);

        List<Object> passes = new ArrayList<>();
        passes.add("Concert backstage passes");
        passes.add(15);
        passes.add(20);


        values.put(1, vest);
        values.put(2, brie);
        values.put(3, elixir);
        values.put(4, sulfuras);
        values.put(5, passes);

        return values;
    }


    public static void getQuality(int item, int daysLeft, int initialQuality, int daysFromNow) {

        int quality = 0;
        int expiration = 0;

        switch (item) {
            case 1:
                expiration = totalDaysDecrease(daysLeft, daysFromNow);
                quality = qualityDecrease(initialQuality, daysFromNow);
                System.out.println("The +5 Dexterity Vest will have the quality of " + quality + " in " + daysFromNow + " days.");
                break;
//            case 2:
//                quality = qualityIncrease(daysFromNow);
//                break;
//            case 3:
//                quality = qualityIncrease(daysFromNow);
//                break;
//            case 4:
//                quality = qualityIncrease(daysFromNow);
//                break;
//            case 5:
//                quality = qualityIncrease(daysFromNow);
//                break;
//            default:
//                quality =;
        }

    }


    public static int qualityIncrease(int initialQuality, int daysFromNow) {
        int totalQuality = initialQuality + daysFromNow;

        if(totalQuality > 50) {
            return 50;
        }
        return totalQuality;
    }

    public static int qualityDecrease(int initialQuality, int daysFromNow) {
        int totalQuality = initialQuality - daysFromNow;

        if(totalQuality <= 0) {
            return 0;
        }
        return totalQuality;
    }

    public static int totalDaysIncrease(int daysLeft, int daysFromNow) {
        return daysLeft + daysFromNow;
    }

    public static int totalDaysDecrease(int daysLeft, int daysFromNow) {
        return daysLeft - daysFromNow;
    }
}
